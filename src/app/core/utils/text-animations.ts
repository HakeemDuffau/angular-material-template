import {
  animate,
  keyframes,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';

export const fade = trigger('fadeAnimation', [
  transition(':enter', [
    style({ opacity: 0 }),
    animate('200ms', keyframes([style({ opacity: 1 })])),
  ]),

  transition(':leave', [
    style({ opacity: 1 }),
    animate('200ms', keyframes([style({ opacity: 0 })])),
  ]),
]);
