import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { delay, map, shareReplay, startWith } from 'rxjs/operators';
import { Page } from '../models/page';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss'],
})
export class NavigationComponent implements OnInit {
  pages: Page[];
  isExpanded: boolean = true;

  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(
      map((result) => result.matches),
      startWith(true),
      delay(0),
      shareReplay()
    );

  constructor(private breakpointObserver: BreakpointObserver) {}
  ngOnInit(): void {
    this.pages = [
      { name: 'Home', icon: 'home', route: '' },
      { name: 'Collaborators', icon: 'supervisor_account', route: '' },
      { name: 'Tasks', icon: 'task', route: '' },
    ];
  }

  onResize() {
    this.isExpanded = !this.isExpanded;
  }
}
